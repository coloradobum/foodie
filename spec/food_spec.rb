require 'foodie'

describe Foodie::Food do
  it "pluaralizes a word" do
    Foodie::Food.pluralize("Tomato").should eql("Tomatoes")
  end
end
