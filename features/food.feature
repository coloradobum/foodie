Feature: Food
  In order to portray or pluralize food
  As a CLI
  I want to be as objective as possible

  #In the When I run steps, the first word inside the quotes is the name of our executable, the second is the task name, and any further text is arguments or options.

  Scenario: Broccoli is gross
    When I run `foodie portray broccoli`
    Then the output should contain "Gross!"

  Scenario: Tomato, or Tomato?
    When I run `foodie pluralize --word Tomato`
    Then the output should contain "Tomatoes"
